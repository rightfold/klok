<?php
namespace rightfold\Klok;

class LocalDateTest extends \PHPUnit_Framework_TestCase {
    public function testGetters() {
        $localDate = new LocalDate(
            GregorianCalendar::instance(),
            GregorianCalendar::ANNO_DOMINI,
            2014, 7, 19
        );

        $this->assertSame(GregorianCalendar::instance(), $localDate->calendar());
        $this->assertSame(GregorianCalendar::ANNO_DOMINI, $localDate->era());
        $this->assertSame(2014, $localDate->year());
        $this->assertSame(7, $localDate->month());
        $this->assertSame(19, $localDate->day());
    }
}
