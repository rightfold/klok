<?php
namespace rightfold\Klok;

class InstantTest extends \PHPUnit_Framework_TestCase {
    public function testFromTicks() {
        $instant = Instant::fromTicks(80000);
        $this->assertSame(80000, $instant->ticks());
    }

    public function testEpoch() {
        $epoch = Instant::epoch();
        $this->assertSame(0, $epoch->ticks());
    }
}
