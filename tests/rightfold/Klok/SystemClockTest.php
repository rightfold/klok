<?php
namespace rightfold\Klok;

class SystemClockTest extends \PHPUnit_Framework_TestCase {
    public function testSystemClock() {
        $clock = SystemClock::instance();
        $this->assertEquals(
            microtime(true) * 1000 * Instant::TICKS_PER_MILLISECOND,
            $clock->now()->ticks(),
            null,
            Instant::TICKS_PER_MILLISECOND
        );
    }
}
