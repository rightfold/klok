<?php
namespace rightfold\Klok;

class GregorianCalendarTest extends \PHPUnit_Framework_TestCase {
    public function testFirstYearOfEra() {
        $this->assertSame(1, GregorianCalendar::instance()->firstYearOfEra(GregorianCalendar::BEFORE_CHRIST));
        $this->assertSame(1, GregorianCalendar::instance()->firstYearOfEra(GregorianCalendar::ANNO_DOMINI));
    }

    public function testMonthsInYear() {
        $this->assertSame(12, GregorianCalendar::instance()->monthsInYear(GregorianCalendar::ANNO_DOMINI, 1));
    }

    public function testDaysInMonth() {
        $calendar = GregorianCalendar::instance();
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 0));
        // February is tested in testLeapYears().
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 2));
        $this->assertSame(30, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 3));
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 4));
        $this->assertSame(30, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 5));
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 6));
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 7));
        $this->assertSame(30, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 8));
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 9));
        $this->assertSame(30, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 10));
        $this->assertSame(31, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1, 11));
    }

    public function testLeapYears() {
        $calendar = GregorianCalendar::instance();
        $this->assertSame(28, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1900, 1));
        $this->assertSame(28, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1991, 1));
        $this->assertSame(29, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 1996, 1));
        $this->assertSame(29, $calendar->daysInMonth(GregorianCalendar::ANNO_DOMINI, 2000, 1));
    }
}
