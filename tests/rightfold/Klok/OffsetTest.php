<?php
namespace rightfold\Klok;

class OffsetTest extends \PHPUnit_Framework_TestCase {
    public function testGetters() {
        $offset = Offset::fromTicks(42);
        $this->assertSame(42, $offset->ticks());
    }
}
