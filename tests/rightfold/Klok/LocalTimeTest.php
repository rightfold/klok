<?php
namespace rightfold\Klok;

class LocalTimeTest extends \PHPUnit_Framework_TestCase {
    public function testNegativeHour() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(-1);
    }

    public function testTooLargeHour() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(24);
    }

    public function testNegativeMinute() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(0, -1);
    }

    public function testTooLargeMinute() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(0, 60);
    }

    public function testNegativeSecond() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(0, 0, -1);
    }

    public function testTooLargeSecond() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(0, 0, 60);
    }

    public function testNegativeMillisecond() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(0, 0, 0, -1);
    }

    public function testTooLargeMillisecond() {
        $this->setExpectedException('InvalidArgumentException');
        new LocalTime(0, 0, 0, 1000);
    }

    public function testGetters() {
        $localTime = new LocalTime(1, 2, 3, 4);
        $this->assertSame(1, $localTime->hour());
        $this->assertSame(2, $localTime->minute());
        $this->assertSame(3, $localTime->second());
        $this->assertSame(4, $localTime->millisecond());
    }
}
