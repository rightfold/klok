<?php
namespace rightfold\Klok;

class LocalDateTimeTest extends \PHPUnit_Framework_TestCase {
    public function testGetters() {
        $localDateTime = new LocalDateTime(
            new LocalDate(GregorianCalendar::instance(), GregorianCalendar::ANNO_DOMINI, 1, 2, 3),
            new LocalTime(4, 5, 6)
        );
        $this->assertSame(GregorianCalendar::instance(), $localDateTime->date()->calendar());
        $this->assertSame(GregorianCalendar::ANNO_DOMINI, $localDateTime->date()->era());
        $this->assertSame(1, $localDateTime->date()->year());
        $this->assertSame(2, $localDateTime->date()->month());
        $this->assertSame(3, $localDateTime->date()->day());
        $this->assertSame(4, $localDateTime->time()->hour());
        $this->assertSame(5, $localDateTime->time()->minute());
        $this->assertSame(6, $localDateTime->time()->second());
    }
}
