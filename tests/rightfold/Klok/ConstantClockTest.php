<?php
namespace rightfold\Klok;

class ConstantClockTest extends \PHPUnit_Framework_TestCase {
    public function testConstantClock() {
        $clock = new ConstantClock(Instant::fromTicks(1000));
        $this->assertSame(1000, $clock->now()->ticks());
    }
}
