<?php
namespace rightfold\Klok;

class JulianCalendarTest extends \PHPUnit_Framework_TestCase {
    public function testFirstYearOfEra() {
        $this->assertSame(1, JulianCalendar::instance()->firstYearOfEra(JulianCalendar::BEFORE_CHRIST));
        $this->assertSame(1, JulianCalendar::instance()->firstYearOfEra(JulianCalendar::ANNO_DOMINI));
    }

    public function testMonthsInYear() {
        $this->assertSame(12, JulianCalendar::instance()->monthsInYear(JulianCalendar::ANNO_DOMINI, 1));
    }

    public function testDaysInMonth() {
        $calendar = JulianCalendar::instance();
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 0));
        // February is tested in testLeapYears().
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 2));
        $this->assertSame(30, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 3));
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 4));
        $this->assertSame(30, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 5));
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 6));
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 7));
        $this->assertSame(30, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 8));
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 9));
        $this->assertSame(30, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 10));
        $this->assertSame(31, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1, 11));
    }

    public function testLeapYears() {
        $calendar = JulianCalendar::instance();
        $this->assertSame(29, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1900, 1));
        $this->assertSame(28, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1991, 1));
        $this->assertSame(29, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 1996, 1));
        $this->assertSame(29, $calendar->daysInMonth(JulianCalendar::ANNO_DOMINI, 2000, 1));
    }
}
