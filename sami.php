<?php

return new Sami\Sami(__DIR__ . '/src', [
    'title' => 'Klok',
    'default_opened_level' => 2,
    'build_dir' => __DIR__ . '/doc',
    'cache_dir' => '/tmp/sami-cache',
]);
