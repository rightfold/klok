<?php
namespace rightfold\Klok;

/**
 * Represents a time of day.
 */
class LocalTime {
    const HOURS_PER_STANDARD_DAY = 24;
    const MINUTES_PER_HOUR = 60;
    const SECONDS_PER_MINUTE = 60;
    const MILLISECONDS_PER_SECOND = 1000;

    private $hour, $minute, $second, $millisecond;

    public function __construct($hour = 0, $minute = 0, $second = 0, $millisecond = 0) {
        if ($hour < 0 || $hour >= self::HOURS_PER_STANDARD_DAY
            || $minute < 0 || $minute >= self::MINUTES_PER_HOUR
            || $second < 0 || $second >= self::SECONDS_PER_MINUTE
            || $millisecond < 0 || $millisecond >= self::MILLISECONDS_PER_SECOND
        ) {
            throw new \InvalidArgumentException();
        }

        $this->hour = $hour;
        $this->minute = $minute;
        $this->second = $second;
        $this->millisecond = $millisecond;
    }

    public function hour() {
        return $this->hour;
    }

    public function minute() {
        return $this->minute;
    }

    public function second() {
        return $this->second;
    }

    public function millisecond() {
        return $this->millisecond;
    }
}
