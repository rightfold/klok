<?php
namespace rightfold\Klok;

/**
 * Represents a clock that always returns the same time.
 */
final class ConstantClock implements Clock {
    private $now;

    public function __construct(Instant $now) {
        $this->now = $now;
    }

    public function now() {
        return $this->now;
    }
}
