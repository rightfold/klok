<?php
namespace rightfold\Klok;

/**
 * Represents an offset from UTC.
 */
final class Offset {
    private $ticks;

    private function __construct($ticks) {
        $this->ticks = $ticks;
    }

    public static function fromTicks($ticks) {
        return new Offset($ticks);
    }

    public function ticks() {
        return $this->ticks;
    }
}
