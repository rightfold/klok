<?php
namespace rightfold\Klok;

/**
 * Represents a date with a time but without a timezone.
 */
final class LocalDateTime {
    private $date, $time;

    public function __construct(LocalDate $date, LocalTime $time) {
        $this->date = $date;
        $this->time = $time;
    }

    public function date() {
        return $this->date;
    }

    public function time() {
        return $this->time;
    }
}
