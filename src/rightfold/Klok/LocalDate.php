<?php
namespace rightfold\Klok;

/**
 * Represents a date without a timezone.
 */
final class LocalDate {
    private $calendar, $era, $year, $month, $day;

    public function __construct(Calendar $calendar, $era, $year, $month, $day) {
        // TODO: Validate era, year, month and day
        $this->calendar = $calendar;
        $this->era = $era;
        $this->year = $year;
        $this->month = $month;
        $this->day = $day;
    }

    public function calendar() {
        return $this->calendar;
    }

    public function era() {
        return $this->era;
    }

    public function year() {
        return $this->year;
    }

    public function month() {
        return $this->month;
    }

    public function day() {
        return $this->day;
    }
}
