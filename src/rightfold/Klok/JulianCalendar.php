<?php
namespace rightfold\Klok;

/**
 * Represents the Julian calendar.
 */
final class JulianCalendar implements Calendar {
    use JulianCalendarLike;

    const BEFORE_CHRIST = 0;
    const ANNO_DOMINI = 1;

    private static $instance = null;

    private function __construct() { }

    public static function instance() {
        if (self::$instance === null) {
            self::$instance = new JulianCalendar();
        }
        return self::$instance;
    }

    public function isLeapYear($era, $year) {
        return $year % 4 === 0;
    }
}
