<?php
namespace rightfold\Klok;

/**
 * Represents a clock that can return the current time.
 */
interface Clock {
    /**
     * @return Instant the current time according to this clock.
     */
    public function now();
}
