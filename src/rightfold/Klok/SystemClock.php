<?php
namespace rightfold\Klok;

final class SystemClock implements Clock {
    private static $instance = null;

    private function __construct() { }

    /**
     * @return SystemClock returns the system clock.
     */
    public static function instance() {
        if (self::$instance === null) {
            self::$instance = new SystemClock();
        }
        return self::$instance;
    }

    public function now() {
        return Instant::fromTicks((int)(microtime(true) * 1000 * Instant::TICKS_PER_MILLISECOND));
    }
}
