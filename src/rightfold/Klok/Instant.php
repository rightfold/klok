<?php
namespace rightfold\Klok;

/**
 * Represents a moment in time.
 */
final class Instant {
    const TICKS_PER_MILLISECOND = 1;

    private $ticks;

    private function __construct($ticks) {
        $this->ticks = $ticks;
    }

    /**
     * @param int $ticks The number of ticks since the Unix epoch.
     * @return Instant the instant representing `$ticks` ticks since the Unix
     *                 epoch.
     */
    public static function fromTicks($ticks) {
        return new Instant($ticks);
    }

    /**
     * @return Instant the instant representing the Unix epoch.
     */
    public static function epoch() {
        return self::fromTicks(0);
    }

    /**
     * @return int the number of ticks since the Unix epoch represented by this
     *             instant.
     */
    public function ticks() {
        return $this->ticks;
    }
}
