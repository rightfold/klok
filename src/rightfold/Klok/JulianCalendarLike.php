<?php
namespace rightfold\Klok;

/**
 * Trait that implements all methods in the `Calendar` interface, with 12
 * months per year and 30 or 31 days per month as in the Julian calendar,
 * except for the second month, which has 28 days if `isLeapYear` returns
 * `false` and 29 days if it returns `true`.
 */
trait JulianCalendarLike {
    private static $daysPerMonth = [
        0 => 31,
        2 => 31,
        3 => 30,
        4 => 31,
        5 => 30,
        6 => 31,
        7 => 31,
        8 => 30,
        9 => 31,
        10 => 30,
        11 => 31,
    ];

    public function firstYearOfEra($era) {
        return 1;
    }

    public function yearsInEra($era) {
        return 50000;
    }

    public function monthsInYear($era, $year) {
        return 12;
    }

    public function daysInMonth($era, $year, $month) {
        if ($month === 1) {
            return $this->isLeapYear($era, $year) ? 29 : 28;
        } else {
            return self::$daysPerMonth[$month];
        }
    }

    public abstract function isLeapYear($era, $year);
}
