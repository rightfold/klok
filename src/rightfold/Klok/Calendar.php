<?php
namespace rightfold\Klok;

// TODO: Document exceptions thrown by these methods.

interface Calendar {
    public function firstYearOfEra($era);
    public function yearsInEra($era);
    public function monthsInYear($era, $year);
    public function daysInMonth($era, $year, $month);
}
