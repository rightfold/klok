<?php
require_once __DIR__ . '/../vendor/autoload.php';
use rightfold\Klok\JulianCalendar;
use rightfold\Klok\GregorianCalendar;

if (isset($_GET['year']) && isset($_GET['calendar'])) {
    $year = (int)$_GET['year'];

    switch ($_GET['calendar']) {
        case 'julian-calendar':
            $calendar = JulianCalendar::instance();
            break;

        case 'gregorian-calendar':
            $calendar = GregorianCalendar::instance();
            break;

        default:
            die('invalid input');
    }

    $class = get_class($calendar);

    switch ($_GET['era']) {
        case 'bc':
            $era = $class::BEFORE_CHRIST;
            break;

        case 'ad':
            $era = $class::ANNO_DOMINI;
            break;

        default:
            die('invalid input');
    }

    $isLeapYear = $calendar->isLeapYear($era, $year);
}

?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Is it a leap year?</title>
    </head>
    <body>
        <form action="" method="GET">
            <input name="year" type="number" value="<?php if (isset($_GET['year'])) echo $_GET['year']; ?>">
            <select name="era">
                <option <?php if (isset($_GET['era']) && $_GET['era'] === 'bc') echo 'selected'; ?> value="bc">BC</option>
                <option <?php if (isset($_GET['era']) && $_GET['era'] === 'ad') echo 'selected'; ?> value="ad">AD</option>
            </select>
            <select name="calendar">
                <option <?php if (isset($_GET['calendar']) && $_GET['calendar'] === 'julian-calendar') echo 'selected'; ?> value="julian-calendar">Julian calendar</option>
                <option <?php if (isset($_GET['calendar']) && $_GET['calendar'] === 'gregorian-calendar') echo 'selected'; ?>  value="gregorian-calendar">Gregorian calendar</option>
            </select>
            <input type="submit">
        </form>
        <?php if (isset($isLeapYear)): ?>
            <?php echo "$year " . strtoupper($_GET['era']) . ' is ' . ($isLeapYear ? 'a' : 'not a') . ' leap year!'; ?>
        <?php endif; ?>
    </body>
</html>
